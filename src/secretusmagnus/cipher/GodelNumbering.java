package secretusmagnus.cipher;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Set;

import secretusmagnus.utilities.IntegerFactorization;
import secretusmagnus.utilities.PrimeNumberUtilities;

/**
 * This class can be used to encrypt and decrypt messages using the Godel numbering system.
 * */
public class GodelNumbering {
	
	/**
	 * Associate an unique integer to an unique symbol.
	 * */
	private HashMap<Integer, Character> alphabet;
	
	/**
	 * Empty constructor initialize the alphabet with
	 * indexes following the sequence :
	 * U0 = 3
	 * Un = Un-1 + 2
	 * 
	 * Thus, the alphabet will have the following form :
	 * {a : 3, b : 5, c : 7 ...}
	 * */
	public GodelNumbering() {
		alphabet = new HashMap<Integer, Character>();
		initialize_alphabet();
	}
	
	private void initialize_alphabet() {
		int i = 3;
		for(char c = 'a'; c <= 'z' ; ++c) {
			alphabet.put(i, c);
			i += 2;
		}
	}
	
	/**
	 * Encrypt a message using the Godel numbering system.
	 * 
	 * @param the message to encrypt
	 * @exception Exception An exception is thrown is the message contains characters not present in the alphabet.
	 * */
	public BigInteger encrypt(String message) throws Exception {
		BigInteger result = new BigInteger("1");
		int[] prime_numbers = PrimeNumberUtilities.computeFirstNthPrimeNumbers(message.length());
		int i = 0; 
		while(i < message.length() && isInAlphabet(message.charAt(i))) {
			int power = getAlphabetNumber(message.charAt(i));
			BigInteger part = new BigInteger("" + prime_numbers[i]);
			part = part.pow(power);
			result = result.multiply(part);	
			i++;
		}
		if(i < message.length()) {
			throw new Exception("Message contains character missing from the alphabet: " + message.charAt(i));
		}
		return result;
	}
	
	/**
	 * Returns true if the character is in the alphabet, false otherwise.
	 * @param c the character to test.
	 * */
	public boolean isInAlphabet(char c) {
		return alphabet.containsValue(c);
	}
	
	/**
	 * Return an index for a character in the alphabet.
	 * @param c The character associated with the index
	 * @exception Exception An exception is thrown if there is no index for that character.
	 * */
	public int getAlphabetNumber(char c) throws Exception {
		Set<Integer> keys = alphabet.keySet();
		for(int i : keys) {
			if(alphabet.get(i) == c) {
				return i;
			}
		}
		throw new Exception("Alphabet doesn't contain character: " + c);
	}
	
	/**
	 * Returns the actual alphabet for that godel numbering system.
	 * */
	public HashMap<Integer, Character> getAlphabet() {
		return this.alphabet;
	}

	/**
	 * Decrypt an integer and if possible find its string equivalent.
	 * 
	 * @param b The BigInteger to decrypt.
	 * @exception Exception An exception is thrown if the BigInteger doesn't correspond to a valid message within the alphabet.
	 * */
	public String decrypt(BigInteger b) {
		String result = "";
		HashMap<Integer, Integer> factors = IntegerFactorization.naiveFactorization(b);
		Set<Integer> keys = factors.keySet();
		for(int i : keys) {
			int power = factors.get(i);
			char c = alphabet.get(power);
			result += c;
		}
		return result;
	}
}
