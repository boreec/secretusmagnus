package secretusmagnus.cipher;

import secretusmagnus.utilities.CharacterUtilities;
import secretusmagnus.utilities.StringUtilities;

/**
 * This class gathers methods to use a Vigenere cipher.
 * This cipher system is a substitution where the character
 * is replaced by another, according to a key (a string) and its
 * position in the message.
 * */
public class VigenereCipher {
	
	private int[][] vigenereMatrix;
	
	/**
	 * Empty constructor will initialize a vigenere matrix
	 * with default values.
	 * */
	public VigenereCipher() {
		initializeVigenereMatrix();
	}
	
	private void initializeVigenereMatrix() {
		vigenereMatrix = new int[26][26];
		for(int i = 0; i < vigenereMatrix.length; i++) {
			for(int j = 0; j < vigenereMatrix.length; j++) {
				vigenereMatrix[i][j] = (i + j) % 26;
			}
		}
	}
	
	/**
	 * Encrypt a message. Every character in the plaintext message
	 * will be replaced by another one, according to a key and its position
	 * in the message.
	 * 
	 * @param plaintext The plain text to encrypt.
	 * @param key The key to use for the encryption.
	 * 
	 * @return The cipher text message.
	 * */
	public String encrypt(String plaintext, String key) {
		String result = "";
		String repeated_key = StringUtilities.repeatWordUntilNthChar(key, plaintext.length());
		for(int i = 0; i < plaintext.length(); ++i) {
			char plaintext_char = plaintext.charAt(i);
			if(Character.isLetter(plaintext_char)) {
				char key_char = repeated_key.charAt(i);
				int plaintext_letter_pos = CharacterUtilities.getAlphabetCharacterIndex(plaintext_char);
				int key_letter_pos = CharacterUtilities.getAlphabetCharacterIndex(key_char);	
				int vigenere_num = vigenereMatrix[key_letter_pos][plaintext_letter_pos];
				char vigenere_char = CharacterUtilities.getAlphabetCharacter(vigenere_num, Character.isUpperCase(plaintext_char));
				result += vigenere_char;
			}else {
				result += plaintext_char;
			}
		}
		return result;
	}
	
	/**
	 * Decrypt a message that has been encrypted with this
	 * a VigenereSystem.
	 * 
	 * @param ciphertext The text to decrypt.
	 * @param key The key to use to decrypt, it is important that this key
	 * is the same as the one used to encrypt.
	 * @return The original plain text message, before it was encrypted.
	 * */
	public String decrypt(String ciphertext, String key) {
		String result = "";
		String repeated_key = StringUtilities.repeatWordUntilNthChar(key, ciphertext.length());
		for(int i = 0; i < ciphertext.length(); ++i) {
			char ciphertext_char = ciphertext.charAt(i);
			if(Character.isLetter(ciphertext_char)) {
				int line = CharacterUtilities.getAlphabetCharacterIndex(repeated_key.charAt(i));
				char plaintext_char = ciphertext_char;
				for(int j = 0; j < vigenereMatrix[line].length; ++j) {
					char current_char = CharacterUtilities.getAlphabetCharacter(vigenereMatrix[line][j], Character.isUpperCase(ciphertext_char));
					if(current_char == ciphertext_char) {
						plaintext_char = CharacterUtilities.getAlphabetCharacter(j, Character.isUpperCase(ciphertext_char));
						break;
					}
				}
				result += plaintext_char;
			}else {
				result += ciphertext_char;
			}
		}
		return result;
	}
	
	/**
	 * Return the internal Vigenere matrix.
	 * */
	public int[][] getVigenereMatrix() {
		return this.vigenereMatrix;
	}
	
	/**
	 * Create a string representing the Vigenere matrix where
	 * each character is separated by a tabulation character and
	 * where each line is separated by a newline character.
	 * */
	public String toString() {
		String result = "";
		for(int i = 0; i < this.vigenereMatrix.length; ++i) {
			for(int j = 0; j < this.vigenereMatrix[i].length; ++j) {
				result += "\t" + this.vigenereMatrix[i][j];
			}
			result += "\n";
		}
		return result;
	}
}
