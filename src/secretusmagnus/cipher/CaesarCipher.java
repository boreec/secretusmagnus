package secretusmagnus.cipher;
import secretusmagnus.utilities.CharacterUtilities;

/**
 * This class gathers methods to encrypt/decrypt using the 
 * Caesar cipher system. In that cipher system, every letter
 * is shifted to another in the alphabet.
 * */
public class CaesarCipher {
	
	/**
	 * Encrypt a message using the Caesar cipher system.
	 * The encryption will shift every characters to another one
	 * according to a value. For example, if the shifting value is 2,
	 * then every letter in the message will be replaced with the letter
	 * 2 position after itself in the alphabet :
	 * 'a' becomes 'c', 'b' becomes 'd',... and 'y' becomes 'a', 'z' becomes 'b'
	 * */
	public static String encrypt(String message, int shift) {
		String result = "";
		for(int i = 0; i < message.length(); ++i) {
			char current_char = message.charAt(i);
			if(Character.isLetter(current_char)){
				int index = CharacterUtilities.getAlphabetCharacterIndex(current_char);
				index = (index + shift) % 26;
				char shifted_char = CharacterUtilities.getAlphabetCharacter(index, Character.isUpperCase(current_char));
				result += shifted_char;
			}else {
				result += current_char;
			}
		}
		return result;
	}
	
	/**
	 * Decrypt a message that has been encrypted with the Caesar cipher with a
	 * specific shift. If the message is "khoor" and the shift is 3, then the decrypted
	 * message is "hello".
	 * 
	 * @param message the encrypted message to decrypt.
	 * @param shift The shifting value used for the encryption.
	 * */
	public static String decrypt(String message, int shift) {
		String result = "";
		for(int i = 0; i< message.length(); ++i) {
			char current_char = message.charAt(i);
			if(Character.isLetter(current_char)) {;
				int index = CharacterUtilities.getAlphabetCharacterIndex(current_char);
				index = (index - shift) % 26;
				if(index < 0) {
					index = 26 + index;
				}
				char shifted_char = CharacterUtilities.getAlphabetCharacter(index, Character.isUpperCase(current_char));
				result += shifted_char;
			}else {
				result += current_char;
			}
		}
		return result;
	}
}
