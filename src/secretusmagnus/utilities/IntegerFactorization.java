package secretusmagnus.utilities;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Set;

/**
 * This class gathers many useful functions to factorize an integer into its prime factors.
 * */
public class IntegerFactorization {
	
	/**
	 * Compute every prime factors for an integer using a naive algorithm.
	 * This algorithm is very stupid. It will check from 1 to N if the current index
	 * is divisor for N. If so it is add to a dictionary data structure (a HashMap),
	 * otherwise it's incremented by one and the divisor test is checked again.
	 * It means in the worst case, N comparisons are made.	For big N, the time might
	 * be too big to compute. However with an integer type, the maximum value is 
	 * 2 147 483 647, the calculation is quite fast. For bigger numbers, see the other
	 * naiveFactorization with BigInteger parameter.
	 * 
	 * @param n The integer to factorize.
	 * @return HashMap<Integer, Integer> where the key is an Integer (a prime factor), and the
	 * 	value is also an Integer (the power of that factor).
	 * */
	public static HashMap<Integer, Integer> naiveFactorization(int n){
		HashMap<Integer, Integer> result = new HashMap <Integer, Integer>();
		
		while (n != 1) {
			int i = 2;
			while(i < n && n % i != 0) {
				i++;
			}
			if(result.containsKey(i)) {
				int current_value = result.get(i);
				current_value++;
				result.put(i, current_value);
			}else {
				result.put(i, 1);
			}
			n = n / i;
		}
		return result;
	}
	
	/**
	 * Compute every prime factors for an integer using a naive algorithm.
	 * This algorithm is very stupid. It will check from 1 to N if the current index
	 * is divisor for N. If so it is add to a dictionary data structure (a HashMap),
	 * otherwise it's incremented by one and the divisor test is checked again.
	 * It means in the worst case, N comparisons are made.	For big N, the time might
	 * be too big to compute. It's better to use the other naiveFactorization method 
	 * if the integer is less than 2 147 483 647 (this method may be really slow for big N).
	 * 
	 * @param n The BigInteger to factorize.
	 * @return HashMap<Integer, Integer> where the key is an Integer (a prime factor), and the
	 * 	value is also an Integer (the power of that factor).
	 * */
	public static HashMap<Integer, Integer> naiveFactorization(BigInteger n){
		HashMap<Integer, Integer> result = new HashMap <Integer, Integer>();
		
		while (n.compareTo(new BigInteger("1")) != 0) {
			BigInteger i = new BigInteger("2");
			while(i.compareTo(n) == -1) {
				if(n.mod(i).compareTo(new BigInteger("0")) == 0) {
					break;
				}
				i = i.add(new BigInteger("1"));
			}
			if(result.containsKey(i.intValue())) {
				int current_value = result.get(i.intValue());
				current_value++;
				result.put(i.intValue(), current_value);
			}else {
				result.put(i.intValue(), 1);
			}

			n = n.divide(i);

		}
		return result;
	}
	
	/**
	 * Compute the prime factors of an integer an display them in an equation form
	 * in a string with the following format :
	 * 
	 * N = (x1^y1)(x1^y2)(...)(xn^yn)
	 * 
	 * xi is a prime factor, yi is its exponent value.
	 * 
	 * @param N the integer to get the result from.
	 * */
	public static String numberToPrimeFactorizationString(int N) {
		String result = "N = ";
		HashMap<Integer, Integer> factors = IntegerFactorization.naiveFactorization(N);
		Set<Integer> keys = factors.keySet();
		for(int k : keys) {
			result += "(" + k + "^" + factors.get(k) + ")";
		}
		return result;
	}
	
	/**
	 * Compute the prime factors of an integer an display them in an equation form
	 * in a string with the following format :
	 * 
	 * N = (x1^y1)(x1^y2)(...)(xn^yn)
	 * 
	 * xi is a prime factor, yi is its exponent value.
	 * 
	 * @param N the BigInteger to get the result from.
	 * */
	public static String numberToPrimeFactorizationString(BigInteger N) {
		String result = "N = ";
		HashMap<Integer, Integer> factors = IntegerFactorization.naiveFactorization(N);
		Set<Integer> keys = factors.keySet();
		for(int k : keys) {
			result += "(" + k + "^" + factors.get(k) + ")";
		}
		return result;
	}

}
