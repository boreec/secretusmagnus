package secretusmagnus.utilities;

/**
 * This class is only a gathering about prime numbers
 * and methods about them.
 * */
public class PrimeNumberUtilities {
	
	/**
	 * This method will compute the first n-ths prime numbers
	 * and return them in an integer array.
	 * 
	 * @param n The number of prime numbers to generate
	 * */
	public static int[] computeFirstNthPrimeNumbers(int n) {
		int[] prime_numbers = new int[n];
		int previous_prime = 0;
		for(int i = 0; i < n; ++i) {
			int num;
			if(i > 0) {
				num = previous_prime + 1;
			}else {
				num = 2;
			}
			while(!isPrime(num)) {
				num++;
			}
			previous_prime = num;
			prime_numbers[i] = num;
		}
		return prime_numbers;
	}
	
	/**
	 * Check if an integer is a prime number or not.
	 * 
	 * @param n The integer to test.
	 * */
	public static boolean isPrime(int n) {
		boolean prime = true;
		int i = 2;
		if(n <= 1) {
			prime = false;
		}
		while(i < n && prime) {
			if(n % i == 0) {
				prime = false;
			}
			i++;
		}
		return prime;	
	}
}
