package secretusmagnus.utilities;

/**
 * This class gathers some useful methods to operate
 * on char attributes.
 * */
public class CharacterUtilities {
	
	/**
	 * Returns a character corresponding to an alphabet position.
	 * For example if the alphabet position is 0, then the character
	 * returned is 'a' or 'A'. If the index is not a good alphabet position,
	 * the character returned is 0x0.
	 * 
	 * @param i The alphabet position to get the character.
	 * @param uppercase A boolean to tell if the returned character must be uppercase or not.
	 * */
	public static char getAlphabetCharacter(int i, boolean uppercase) {
		int index = 0;
		if(uppercase) {
			index = (int)('A');
		}else {
			index = (int)('a');
		}
		return (char)(index + i);
	}
	
	/**
	 * Determines a letter position in the alphabet. If the character
	 * provided is a letter (lowercase or uppercase), an integer representing
	 * its index in the alphabet is returned.
	 * For example if c = 'a', then the index is 0. 'b' is 1, 'c' is 2 and so on.
	 * If the character provided is not in the alphabet, -1 is returned.
	 * 
	 * @param c The character to determine the alphabetical index.
	 * */
	public static int getAlphabetCharacterIndex(char c) {
		int c_value = (int)(c);
		int index = -1;
		if(c_value >= (int)('a') && c_value <= (int)('z')) {
			index = c_value - (int)('a');
		}
		if(c_value >= (int)('A') && c_value <= (int)('Z')) {
			index = c_value - (int)('A');
		}
		return index;
	}
}
