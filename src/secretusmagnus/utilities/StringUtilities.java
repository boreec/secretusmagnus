package secretusmagnus.utilities;

/**
 * This class gather some useful methods about strings.
 * */
public class StringUtilities {
	
	/**
	 * Take a word an repeat it to reach an index. If the index
	 * is less than the word length, a substring is returned.
	 * 
	 * For example for the word "lemon" and the index 2, the
	 * returned value is "lem". For the index 12, the result is
	 * "lemonlemonle".
	 * 
	 * @param word The word to repeat.
	 * @param idx The index to reach.
	 * */
	public static String repeatWordUntilNthChar(String word, int idx) {
		String result = "";
		if(idx < word.length()) {
			result = word.substring(0,idx);
		}else {
			int times = idx / word.length();
			int remainder = idx % word.length();
			for(int i = 0; i < times; ++i) {
				result += word;
			}
			if(remainder != 0) {
				result += word.substring(0,remainder);
			}
		}
		return result;
	}
}
