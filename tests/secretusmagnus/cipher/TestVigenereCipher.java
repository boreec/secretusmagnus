package secretusmagnus.cipher;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestVigenereCipher {

	@Test
	public void testVigenereMatrixHasGoodSize() {
		VigenereCipher v = new VigenereCipher();
		int[][] matrix = v.getVigenereMatrix();
		assertEquals(26, matrix.length);
		for(int i = 0; i < 26; i++) {
			assertEquals(26, matrix[i].length);
		}
	}
	
	@Test
	public void testVigenereMatrixHasOnlyElementsBetweenZeroAndTwentyFive() {
		VigenereCipher v = new VigenereCipher();
		int[][] matrix = v.getVigenereMatrix();
		for(int i = 0; i < matrix.length; i++) {
			for(int j = 0; j < matrix[i].length; j++) {
				assertTrue(matrix[i][j] >= 0 && matrix[i][j] < 26); 
			}
		}
	}
	
	@Test
	public void testEncrypt() {
		VigenereCipher v = new VigenereCipher();
		assertEquals("LXFOPVEFRNHR", v.encrypt("ATTACKATDAWN","lemon"));
		assertEquals("lxfopvefrnhr", v.encrypt("attackatdawn","lemon"));
	}
	
	@Test
	public void testDecrypt() {
		VigenereCipher v = new VigenereCipher();
		assertEquals("ATTACKATDAWN", v.decrypt("LXFOPVEFRNHR","lemon"));
		assertEquals("attackatdawn", v.decrypt("lxfopvefrnhr","lemon"));
	}
}
