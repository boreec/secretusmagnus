package secretusmagnus.cipher;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Set;

import org.junit.jupiter.api.Test;

import secretusmagnus.cipher.GodelNumbering;

class TestGodelNumbering {
	
	@Test
	public void testInitializeAlphabet() {
		GodelNumbering g = new GodelNumbering();
		HashMap<Integer, Character> alphabet = g.getAlphabet();
		Set<Integer> keys = alphabet.keySet();
		for(int i : keys) {
			assertTrue(i % 2 == 1);
			assertTrue(alphabet.get(i) >= 'a' && alphabet.get(i) <= 'z');
		}
	}
	
	@Test
	public void testEncrypt() throws Exception {
		GodelNumbering g = new GodelNumbering();
		BigInteger hello = new BigInteger("1781222108160036717502249433080"
										+	"000170034183127751862538129648"
										+	"437500000000000000000");
		
		assertEquals(hello, g.encrypt("hello"));
	}
	
	@Test
	public void testDecrypt() {
		GodelNumbering g = new GodelNumbering();
		BigInteger hello = new BigInteger("1781222108160036717502249433080"
				+	"000170034183127751862538129648"
				+	"437500000000000000000");
		
		assertEquals("hello",g.decrypt(hello));
	}
}
