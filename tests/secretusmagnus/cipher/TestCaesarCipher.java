package secretusmagnus.cipher;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import secretusmagnus.cipher.CaesarCipher;

class TestCaesarCipher {

	@Test
	public void testEncrypt() {
		assertEquals("a", CaesarCipher.encrypt("a", 0));
		assertEquals("b", CaesarCipher.encrypt("a", 1));
		assertEquals("c", CaesarCipher.encrypt("a", 2));
		assertEquals("d", CaesarCipher.encrypt("a", 3));
		assertEquals("e", CaesarCipher.encrypt("a", 4));
		assertEquals("z", CaesarCipher.encrypt("z", 0));
		assertEquals("a", CaesarCipher.encrypt("z", 1));
		assertEquals("b", CaesarCipher.encrypt("z", 2));
		assertEquals("c", CaesarCipher.encrypt("z", 3));
		assertEquals("y", CaesarCipher.encrypt("z", -1));
		assertEquals("hello", CaesarCipher.encrypt("hello", 0));
		assertEquals("ZLNLSHGLD O'HQFBFORSHGLH OLEUH",CaesarCipher.encrypt("WIKIPEDIA L'ENCYCLOPEDIE LIBRE", 3));
	}

	@Test
	public void testDecrypt() {
		assertEquals("a",CaesarCipher.decrypt("a", 0));
		assertEquals("a",CaesarCipher.decrypt("b", 1));
		assertEquals("a",CaesarCipher.decrypt("c", 2));
		assertEquals("a",CaesarCipher.decrypt("d", 3));
		assertEquals("a",CaesarCipher.decrypt("e", 4));
		assertEquals("ABCDEFGHIJKLMNOPQRSTUVWXYZ",CaesarCipher.decrypt("DEFGHIJKLMNOPQRSTUVWXYZABC", 3));
		assertEquals("WIKIPEDIA L'ENCYCLOPEDIE LIBRE",CaesarCipher.decrypt("ZLNLSHGLD O'HQFBFORSHGLH OLEUH", 3));
	}
}
