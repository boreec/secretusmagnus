package secretusmagnus.utilities;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestStringUtilities {

	@Test
	public void testRepeatWordUntilNthChar() {
		String word = "lemon";
		assertEquals(word, StringUtilities.repeatWordUntilNthChar(word, word.length()));
		assertEquals("", StringUtilities.repeatWordUntilNthChar(word, 0));
		assertEquals("l", StringUtilities.repeatWordUntilNthChar(word, 1));
		assertEquals("le", StringUtilities.repeatWordUntilNthChar(word, 2));
		assertEquals("lemonlemon", StringUtilities.repeatWordUntilNthChar(word, 2 * word.length()));
		assertEquals("lemonlemonle", StringUtilities.repeatWordUntilNthChar(word, 12));
	}
}
