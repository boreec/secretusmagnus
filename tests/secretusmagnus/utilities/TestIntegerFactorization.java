package secretusmagnus.utilities;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;
import java.util.HashMap;

import org.junit.jupiter.api.Test;

import secretusmagnus.utilities.IntegerFactorization;

class TestIntegerFactorization {

	@Test
	void testNaiveFactorizationWithInteger() {
		int N = 30;
		HashMap<Integer, Integer> factorization = IntegerFactorization.naiveFactorization(N);
		assertEquals(3, factorization.size());
		assertTrue(factorization.containsKey(2));
		assertTrue(factorization.containsKey(3));
		assertTrue(factorization.containsKey(5));
				
		N = 2;
		factorization = IntegerFactorization.naiveFactorization(N);
		assertEquals(1, factorization.size());
		assertTrue(factorization.containsKey(2));
		
		N = 1;
		factorization = IntegerFactorization.naiveFactorization(N);
		assertTrue(factorization.isEmpty());
	}
	
	@Test
	void testNaiveFactorizationWithBigInteger() {
		BigInteger N = new BigInteger("30");
		HashMap<Integer, Integer> factorization = IntegerFactorization.naiveFactorization(N);
		assertEquals(3, factorization.size());
		assertTrue(factorization.containsKey(2));
		assertTrue(factorization.containsKey(3));
		assertTrue(factorization.containsKey(5));
		
		N = new BigInteger("2");
		factorization = IntegerFactorization.naiveFactorization(N);
		assertEquals(1, factorization.size());
		assertTrue(factorization.containsKey(2));
		
		N = new BigInteger("1");
		factorization = IntegerFactorization.naiveFactorization(N);
		assertTrue(factorization.isEmpty());
	}

}
