package secretusmagnus.utilities;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import secretusmagnus.utilities.CharacterUtilities;

class TestCharacterUtilities {

	@Test
	public void testGetAlphabetCharacter() {
		char expectedChar = 'a';
		int expectedValue = 0;
		while(expectedChar <= 'z') {
			assertEquals(expectedChar, CharacterUtilities.getAlphabetCharacter(expectedValue, false));
			expectedChar++;
			expectedValue++;
		}
		
		expectedChar = 'A';
		expectedValue = 0;
		while(expectedChar <= 'Z') {
			assertEquals(expectedChar, CharacterUtilities.getAlphabetCharacter(expectedValue, true));
			expectedChar++;
			expectedValue++;
		}
	}
	
	@Test
	public void testGetAlphabetCharacterIndex() {
		char expectedChar = 'a';
		int expectedValue = 0;
		while(expectedChar <= 'z') {
			assertEquals(expectedValue, CharacterUtilities.getAlphabetCharacterIndex(expectedChar));
			expectedChar++;
			expectedValue++;
		}
		
		expectedChar = 'A';
		expectedValue = 0;
		while(expectedChar <= 'Z') {
			assertEquals(expectedValue, CharacterUtilities.getAlphabetCharacterIndex(expectedChar));
			expectedChar++;
			expectedValue++;
		}
		assertEquals(-1, CharacterUtilities.getAlphabetCharacterIndex('0'));
	}

}
