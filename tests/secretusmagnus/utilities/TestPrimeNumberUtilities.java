package secretusmagnus.utilities;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import secretusmagnus.utilities.PrimeNumberUtilities;

class TestPrimeNumberUtilities {

	@Test
	public void testIsPrime() {
		assertFalse(PrimeNumberUtilities.isPrime(1));
		assertFalse(PrimeNumberUtilities.isPrime(4));
		assertFalse(PrimeNumberUtilities.isPrime(6));
		assertFalse(PrimeNumberUtilities.isPrime(8));
		assertFalse(PrimeNumberUtilities.isPrime(39));
		
		assertTrue(PrimeNumberUtilities.isPrime(2));
		assertTrue(PrimeNumberUtilities.isPrime(3));
		assertTrue(PrimeNumberUtilities.isPrime(5));
		assertTrue(PrimeNumberUtilities.isPrime(7));
		assertTrue(PrimeNumberUtilities.isPrime(11));
		assertTrue(PrimeNumberUtilities.isPrime(17));
	}

	@Test
	public void TestComputeFirstNthPrimeNumbers() {
		int[] result = PrimeNumberUtilities.computeFirstNthPrimeNumbers(5);
		assertEquals(2, result[0]);
		assertEquals(3, result[1]);
		assertEquals(5, result[2]);
		assertEquals(7, result[3]);
		assertEquals(11, result[4]);
	}
}
